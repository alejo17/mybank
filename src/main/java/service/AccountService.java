package service;

import java.util.List;

import repository.AccountRepository;
import domain.Account;

public class AccountService {

	AccountRepository repository;

	public AccountService(AccountRepository repository) {
		this.repository = repository;
	}

	public void createAccount() {
		Account a1 = new Account("1001", 550);
		Account a2 = new Account("1002", 50);
		Account a3 = new Account("1003", 5000);
		Account a4 = new Account("1004", 50000);
		Account a5 = new Account("1005", 5009);
		Account a6 = new Account("1006", 231329);
		Account a7 = new Account("1007", 512349);
		Account a8 = new Account("1008", 121234);
		repository.save(a1);
		repository.save(a2);
		repository.save(a3);
		repository.save(a4);
		repository.save(a5);
		repository.save(a6);
		repository.save(a7);
		repository.save(a8);
				
	}
	public List<Account> cuentas(){
		List<Account> list = repository.findAll();
		return list;
	}
}
