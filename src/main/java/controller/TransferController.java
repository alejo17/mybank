package controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Controller;

import config.WebConfig;
import domain.Account;
import repository.AccountRepository;
import repository.InMemoryAccountRepository;
import service.TransferService;
import service.AccountService;
@Import(WebConfig.class)
@EnableAutoConfiguration
//import domain.Account;

@Controller
public class TransferController {

	@RequestMapping("/banco")
	String home() {	
		System.out.println("Entro");
	     return "banco"; 
	}
	
	@RequestMapping("/cuentas")
	String listarcuentas(ModelMap model) {
		 AccountRepository repository = new InMemoryAccountRepository();
		 AccountService aservice = new AccountService(repository);
		 aservice.createAccount();
		 List<Account> cuentas = aservice.cuentas();
		 model.addAttribute("listacuentas",cuentas);
		 System.out.println(repository.findAll());
	     return "cuentas"; 
	}
	
	@RequestMapping("/nocomplete")
		String nocomplete() {	
			System.out.println("error");
	     return "nocomplete"; 
	}
	
	@RequestMapping("/completed")
	String transfer(ModelMap model, @RequestParam String saccount, @RequestParam String taccount, @RequestParam String amount)
	{
		
	    AccountRepository repository = new InMemoryAccountRepository();
		TransferService service = new TransferService(repository);	
		AccountService aservice = new AccountService(repository);
		
		aservice.createAccount();
		
		double amount_ = Double.parseDouble(amount);
			if(service.transfer(saccount, taccount, amount_ )){
				return "completed";
			}
			return "nocomplete";
			
	}
}
	


